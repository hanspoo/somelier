'use client';

import { Producto } from '../../shared/Producto';

export type DetalleProductoProps = {
  producto: Producto;
};

export function DetalleProducto({ producto }: DetalleProductoProps) {
  return <p>{producto.name}</p>;
}
