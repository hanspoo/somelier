export interface Producto {
  id: any;
  name: string;
  description: string;
  imageURL: string;
  originalPrice: number;
  discountPrice: number;
}
